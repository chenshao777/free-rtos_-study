#include "stm32f10x.h"
#include "led.h"
#include "usart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "SysTick.h"
#include "queue.h"
#include "string.h"

/* 创建队列返回句柄 */
QueueHandle_t Queue_t;

/*
  写队列
*/
int flag1 = 0;
int flag2 = 0;
void TaskSend1(void *pvParameters)
{
	int param;
	BaseType_t status;
	/* 该任务被创建了两个实例，需将传递参数强转为 int 型 */
	param = (int)pvParameters;

	for(;;)
	{
		/* 写队列，参数：目标队列的句柄， 发送数据的指针， 阻塞超时时间*/
		status = xQueueSendToBack(Queue_t, &param, 0);
		
		if(status == pdPASS)
		{
			flag1 = 1;
			flag2 = 0;
		}
		
//		/* 允许其它发送任务执行。 taskYIELD()通知调度器现在就切换到其它任务，而不必等到本任务的时间片耗尽 */ 
		taskYIELD(); 
		
//		/* 不能使用该延时函数，否则当队列满后，只有第一个写任务可以得到调度，第二个写任务会被饿死 */
//		vTaskDelay(1); 
	}
}

/*
  读队列
*/
void TaskRead(void *pvParameters)
{
	int read_data;
	BaseType_t result;
	BaseType_t count;
//	TickType_t xLastExecutionTime;
//	xLastExecutionTime = xTaskGetTickCount(); 
	for(;;)
	{
		flag1 = 0;
		flag2 = 1;
		
		/* 查询队列中数据个数 */
		count = uxQueueMessagesWaiting(Queue_t);
		printf("c = %d\r\n", count);
		
		/* 读队列 */
		result = xQueueReceive(Queue_t, &read_data, 50);
		if(result == pdPASS)
		{
			printf("Read:  %d\r\n", read_data);
		}
		else
			printf("Read NULL\r\n");
		
//		vTaskDelayUntil(&xLastExecutionTime, 10);
//		vTaskDelay(1);
	}
}

/*
  空闲任务：钩子函数
*/
//void vApplicationIdleHook( void ) 
//{ 
///* This hook function does nothing but increment a counter. */ 
//	printf("钩子函数\r\n");
//} 


int main()
{
	SysTick_Init(72);
	USART1_Init(115200);
	printf("串口初始化成功！\r\n");
	
	/*  创建队列, 用于保存最多5个值，每个数据单元都有足够的空间来存储一个 int 型变量  */
	Queue_t = xQueueCreate(20, sizeof(int));
	if(Queue_t != NULL)
		printf("队列创建成功！\r\n");
	
	/* 创建三个任务写队列 */
	xTaskCreate(TaskSend1, "taskSend1", 200, (void*)100, 1, NULL);
	xTaskCreate(TaskSend1, "taskSend2", 200, (void*)110, 1, NULL);
	xTaskCreate(TaskSend1, "taskSend3", 200, (void*)120, 1, NULL);
	
	
	/* 创建一个任务读队列 */
	xTaskCreate(TaskRead, "TaskRead", 200, NULL, 2, NULL);
	
	/* 启动调度器，任务开始执行 */
	vTaskStartScheduler();          //开启任务调度
}

