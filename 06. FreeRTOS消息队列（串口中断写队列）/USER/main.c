#include "stm32f10x.h"
#include "led.h"
#include "usart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "SysTick.h"
#include "queue.h"
#include "string.h"

/* 创建队列返回句柄 */
QueueHandle_t Queue_t;

char rec_buff[100];
int Len;

/*
  读队列
*/
void TaskRead(void *pvParameters)
{
	int read_data;
	BaseType_t result;
	BaseType_t count;
//	TickType_t xLastExecutionTime;
//	xLastExecutionTime = xTaskGetTickCount(); 
	for(;;)
	{
		/* 查询队列中数据个数 */
//		count = uxQueueMessagesWaiting(Queue_t);
//		printf("c = %d\r\n", count);
		
		/* 读队列 */
		result = xQueueReceive(Queue_t, &read_data, portMAX_DELAY);
		if(result == pdPASS)
		{
			printf("%02X \r\n", read_data);
		}
		else
			printf("Read NULL\r\n");
	}
}


int main()
{
	SysTick_Init(72);
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	USART1_Init(115200);
	printf("串口初始化成功！\r\n");
	
	/*  创建队列，每个数据单元都有足够的空间来存储一个 int 型变量  */
	Queue_t = xQueueCreate(100, sizeof(char));
	if(Queue_t != NULL)
		printf("队列创建成功！\r\n");
	
	/* 创建一个任务读队列 */
	xTaskCreate(TaskRead, "TaskRead", 200, NULL, 2, NULL);
	
	/* 启动调度器，任务开始执行 */
	vTaskStartScheduler();          //开启任务调度
}

