#include "stm32f10x.h"
#include "led.h"
#include "usart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "SysTick.h"
#include "dht11.h"
#include "mq2.h"
#include "semphr.h"
#include "string.h"


void systemInit(void);   //各模块初始化

QueueHandle_t xSemaphoreMutex;  //互斥量句柄

/* 使用互斥量标志 */
#define    USE_Mutex_Flag 			1   

/*
 * 任务1一直发送0~99数字
*/
void vSemphrMutex_Task1(void *pvParameters)
{
	const TickType_t xDelay = 50 / portTICK_PERIOD_MS;
	
	BaseType_t result;
	
	for(;;)
	{
		#if (USE_Mutex_Flag == 1)
			/* 上锁 */
			result = xSemaphoreTake(xSemaphoreMutex, 0);
		#endif
		
		if(result == pdFAIL){
			/* 盗锁 */
			xSemaphoreGive(xSemaphoreMutex);
		}
		/* 打印 0 ~ 99 */
		for(int i=0; i < 100 ; i++){
			printf("%d ", i);
		}
		printf("\r\n");
		
		#if (USE_Mutex_Flag == 1)
			/* 开锁 */
			xSemaphoreGive(xSemaphoreMutex);
		#endif
		
		vTaskDelay(xDelay);
	}
}

/*
 * 任务2一直发送100~199数字
*/
void vSemphrMutex_Task2(void *pvParameters)
{
	const TickType_t xDelay = 50 / portTICK_PERIOD_MS;
	
	BaseType_t result;  //获取信号量结果变量
	
	for(;;)
	{
		#if (USE_Mutex_Flag == 1)
			/* 上锁 */
			xSemaphoreTake(xSemaphoreMutex, portMAX_DELAY);
		#endif
		
		/* 打印 100 ~ 199 */
		for(int i=100; i < 200 ; i++){
			printf("%d ", i);
		}
		printf("\r\n");
		
		#if (USE_Mutex_Flag == 1)
			/* 开锁 */
			xSemaphoreGive(xSemaphoreMutex);
		#endif
		
		vTaskDelay(xDelay);
	}
}

int main()
{
	systemInit();   //初始化各个模块

	// 互斥量任务一
	xTaskCreate(vSemphrMutex_Task1, "vSemphrMutex_Task1", 200, NULL, 2, NULL);
	
	// 互斥量任务二
	xTaskCreate(vSemphrMutex_Task2, "vSemphrMutex_Task2", 200, NULL, 2, NULL);
	
	vTaskStartScheduler();          //开启任务调度
}

/*
 * 初始化各个模块
*/
void systemInit(void)
{
	SysTick_Init(72);
	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	USART1_Init(115200);
	printf("串口初始化成功！\r\n");

	/* 创建互斥量 */
	xSemaphoreMutex = xSemaphoreCreateMutex();
}
