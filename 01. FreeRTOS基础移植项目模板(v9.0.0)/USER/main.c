#include "stm32f10x.h"
#include "led.h"
#include "usart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "SysTick.h"

int a[1800] = {0};
char flag1 = 0;
char flag2 = 0;

void Task1(void *pvParameters)
{
	int i;
	const TickType_t xDelay = 2 / portTICK_PERIOD_MS;
	while(1)
	{
		flag1 = 1;
		flag2 = 0;
//		vTaskDelay(xDelay);
		for(i = 0; i < 1800; i++)
			a[i] = 1;
//		printf("111111  %d \r\n", a);
//		vTaskDelay(xDelay);
	}
}

void Task2(void *pvParameters)
{
	int i;
	const TickType_t xDelay = 2 / portTICK_PERIOD_MS;
	while(1)
	{
		flag1 = 0;
		flag2 = 1;
		vTaskDelay(xDelay);
		for(i = 0; i < 1800; i++)
			a[i] = 2;
//		printf("222222  %d\r\n", a);
//		vTaskDelay(xDelay);
	}
}

int main()
{
	SysTick_Init(72);
	USART1_Init(115200);
	printf("串口初始化成功！\r\n");
	
	xTaskCreate(Task1, "task1", 200, NULL, 1, NULL);
	xTaskCreate(Task2, "task2", 200, NULL, 1, NULL);
	
	vTaskStartScheduler();          //开启任务调度
}

