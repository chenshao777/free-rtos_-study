#include "stm32f10x.h"
#include "led.h"
#include "usart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "SysTick.h"
#include "event_groups.h"


EventGroupHandle_t xEventGroupHandle;  //事件组句柄

char flag1;
char flag2;

/*
 等待单个事件
*/
void Task1_Wait_Single(void *pvParameters)
{
//	int i;
	const TickType_t xDelay = 10 / portTICK_PERIOD_MS;
	EventBits_t result;
	while(1)
	{
		flag1 = 1;
		flag2 = 0;
		/* 等待事件 */
		/* 
		1. 等待的事件组
		2. 等待哪些位
		3. 是否清除事件指定位，pdTRUE：清除  pdFALSE：不清除
		4. pdTRUE:等待的位全部为1  pdFALSE:等待的位只有一个为1即可
		5. 阻塞时间 
		*/
		result = xEventGroupWaitBits(xEventGroupHandle, 0x01, pdTRUE, pdTRUE, xDelay);
		/* 
		如果等待超时，则返回bit1为0 
		如果等待成功，则返回bit1为1
		*/
		if(result & 0x01)
			printf("success  %02X\r\n", result);
		else
			printf("fail  %02X\r\n", result);
		
	}
}

/*
 设置事件
*/
void Task2_Set_Event(void *pvParameters)
{
	const TickType_t xDelay = 10 / portTICK_PERIOD_MS;
	while(1)
	{
		flag1 = 0;
		flag2 = 1;
		
		xEventGroupSetBits(xEventGroupHandle, 0x01);   // 设置事件
		printf("111\r\n");

		vTaskDelay(xDelay);
	}
}

int main()
{
	SysTick_Init(72);
	USART1_Init(115200);
	printf("串口初始化成功！\r\n");
	
	/* 创建事件组 */
	xEventGroupHandle = xEventGroupCreate();  
	
	/* 设置事件组 */
	/* 设置事件组中的位
	 * xEventGroup: 哪个事件组
	 * uxBitsToSet: 设置哪些位
	 *              如果uxBitsToSet的bitX, bitY为1, 那么事件组中的bitX, bitY被设置为1
	 *               可以用来设置多个位，比如 0x15 就表示设置bit4, bit2, bit0
	 * 返回值: 返回原来的事件值(没什么意义, 因为很可能已经被其他任务修改了)
	 */
	xEventGroupSetBits(xEventGroupHandle, 0x03);  // 设置bit0和bit1为1
	
	xTaskCreate(Task1_Wait_Single, "Task1_Wait_Single", 200, NULL, 3, NULL);
	xTaskCreate(Task2_Set_Event, "Task2_Set_Event", 200, NULL, 2, NULL);
	
	/* 消耗掉第一次的bit */
	printf("main1  %02x\r\n", xEventGroupWaitBits(xEventGroupHandle, 0x01, pdTRUE, pdTRUE, 0));
	printf("main2  %02x\r\n", xEventGroupWaitBits(xEventGroupHandle, 0x01, pdTRUE, pdTRUE, 0));
	
	vTaskStartScheduler();          //开启任务调度
}

