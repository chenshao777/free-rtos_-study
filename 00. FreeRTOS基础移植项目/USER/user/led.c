#include "led.h"

void LED_Init()
{
	GPIO_InitTypeDef GPIO_InitStruct;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);
//	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOF,ENABLE);
	
	GPIO_InitStruct.GPIO_Pin = LED1_GPIO_Pin;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC,&GPIO_InitStruct);
	
//	GPIO_InitStruct.GPIO_Pin = LED2_GPIO_Pin | LED3_GPIO_Pin;
//	GPIO_Init(GPIOF,&GPIO_InitStruct);
	LED1(ON);
//	LED2(OFF);
//	LED3(OFF);
}

