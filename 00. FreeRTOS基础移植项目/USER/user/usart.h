#ifndef __USART_H_
#define __USART_H_

#include "stm32f10x.h"
#include "stdio.h"


void Usart1_Init(uint32_t brr);
//void Usart_Init(uint32_t brr);
void Usart_SenRec(void);
void Usart_TX(uint8_t data);
uint8_t Usart_RX(void);
void USART1_SendByte(USART_TypeDef* USARTx, uint8_t Data);
void USART1_Send2Byte(USART_TypeDef* USARTx, uint16_t Data);
void USART1_SendArray(USART_TypeDef* USARTx, uint8_t *array);
void USART1_SendStr(USART_TypeDef* USARTx, uint8_t *str);
void Recive_Send(void);

#endif
