#include "usart.h"


void Usart1_Init(uint32_t brr)
{
	USART_InitTypeDef USART_InitStruct;
//	USART_ClockInitTypeDef USART_ClockInitStruct;
	GPIO_InitTypeDef GPIO_InitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;
	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
	
	NVIC_InitStruct.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
	NVIC_Init(&NVIC_InitStruct);
	
	//打开串口GPIO时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	//打开串口外设时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);
	//配置USART TX 的GPIO为推挽复用
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStruct);
	//配置USART RX 的GPIO为浮空输入
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA,&GPIO_InitStruct);
	
	//配置串口的工作参数
	//配置波特率（传入参数）
	USART_InitStruct.USART_BaudRate = brr;
	//配置硬件流控制
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	//配置工作模式，发送和接收
	USART_InitStruct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	//配置校验位
	USART_InitStruct.USART_Parity = USART_Parity_No;  //无奇偶校验
	//配置停止位
	USART_InitStruct.USART_StopBits = USART_StopBits_1;
	//配置数据位
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;
  //完成串口的初始化配置
	USART_Init(USART1,&USART_InitStruct);
	
	USART_ClearFlag(USART1, USART_FLAG_RXNE);	            //清除接收标志位
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);        //开启接收中断
	
	USART_Cmd(USART1,ENABLE);
	
	USART_ITConfig(USART1,USART_IT_RXNE,ENABLE);
}

/*   库函数发送一个字节数据   */
void USART1_SendByte(USART_TypeDef* USARTx, uint8_t Data)
{
	USART_SendData(USARTx,Data);
	while( USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == 0 );
}

/*   库函数发送两个字节数据   */
void USART1_Send2Byte(USART_TypeDef* USARTx, uint16_t Data)
{
	uint8_t temp_h,temp_l;
	temp_h = (Data & 0xff00) >> 8;
	temp_l = Data & 0xff;
	USART_SendData(USARTx,temp_h);
	while( USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == 0 );
	USART_SendData(USARTx,temp_l);
	while( USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == 0 );
}

/*   库函数发送一个数组数据   */
void USART1_SendArray(USART_TypeDef* USARTx, uint8_t *array)
{
	uint8_t i=0;
	while( array[i] != '\0' )
	{
		USART1_SendByte(USARTx, array[i++]);
	}
	while( USART_GetFlagStatus(USARTx, USART_FLAG_TC) == 0 );
}

/*   库函数发送一个字符串   */
void USART1_SendStr(USART_TypeDef* USARTx, uint8_t *str)
{
	do
	{
		USART1_SendByte(USARTx, *str);
	}while( *(++str) != '\0' );
	while( USART_GetFlagStatus(USARTx, USART_FLAG_TC) == 0 );
}

/*  接收数据并返回数据  */
void Recive_Send(void)
{
	if(USART_GetITStatus(USART1,USART_IT_RXNE) == 1)
	{
		USART_SendData(USART1, USART_ReceiveData(USART2));
	}
}


//发送接收
void Usart_SenRec(void)
{
	uint8_t data=0;
	while(!(USART1->SR & (1<<5)));		//等待接收
	data = USART1->DR;
	
	while(!(USART1->SR & (1<<6)));		//等待发送
	USART1->DR = data;
}
//发送一帧字节
void Usart_TX(uint8_t data)
{
	while(!(USART1->SR & (1<<6)));		//等待发送
	USART1->DR = data;
	while(!(USART1->SR & (1<<6)));		//等待发送
}

//接收一帧字节
uint8_t Usart_RX(void)
{
	while(!(USART1->SR & (1<<5)));		//等待接收
	return USART1->DR;
}

//改变数据的输出方向 --往串口输出
int fputc(int c, FILE * stream)
{
	USART1_SendByte(USART1,c);
	return c;
}
//改变数据的输入方向 
int fgetc(FILE * stream)
{
	while(!(USART_GetITStatus(USART1,USART_IT_RXNE)));
	return (int)(USART_ReceiveData(USART1));
}

