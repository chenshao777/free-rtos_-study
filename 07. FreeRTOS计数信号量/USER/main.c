#include "stm32f10x.h"
#include "led.h"
#include "usart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "SysTick.h"
#include "dht11.h"
#include "mq2.h"
#include "semphr.h"
#include "string.h"


void systemInit(void);   //各模块初始化
QueueHandle_t xSemaphore;

QueueHandle_t vSemaphoreCount;  //初始化计数信号量句柄


/*
 * 获取计数信号量任务
*/
void vSemphrCount_Task(void *pvParameters)
{
//	const TickType_t xDelay = 1000 / portTICK_PERIOD_MS;
	BaseType_t result;  //获取信号量结果变量
	while(1)
	{
		/*
		 * portMAX_DELAY：表示没有超时限制
		 * 尝试获取信号量，如果没有获取到则进入阻塞态
		 * 如果设置了超时时间，超时时间内获取到了信号量，则返回pdPASS，如果没有获取到，则返回pdFALSE
		*/
		result = xSemaphoreTake(vSemaphoreCount, portMAX_DELAY);   
		if(result == pdPASS)
			printf("获取到计数信号量！！！\r\n");

	}
}

/*
 * 获取计数信号量任务
*/
void vSemphrCount_Give(void *pvParameters)
{
	const TickType_t xDelay = 50 / portTICK_PERIOD_MS;
	BaseType_t result;  //获取信号量结果变量
	int i;
	while(1)
	{
		/*
		 * portMAX_DELAY：表示没有超时限制
		 * 尝试获取信号量，如果没有获取到则进入阻塞态
		 * 如果设置了超时时间，超时时间内获取到了信号量，则返回pdPASS，如果没有获取到，则返回pdFALSE
		*/
		for(i=0;i<1;i++){
			result = xSemaphoreGive(vSemaphoreCount);   
			if(result == pdPASS)
				printf("释放计数信号量：%d\r\n",i);
			else
				printf("释放失败\r\n");
		}
		
		vTaskDelay(xDelay);
	}
}

int main()
{
	systemInit();   //初始化各个模块

	// 获取计数信号量任务 
	xTaskCreate(vSemphrCount_Task, "vSemphrCount_Task", 200, NULL, 1, NULL);
	
	// 释放计数信号量任务 
	xTaskCreate(vSemphrCount_Give, "vSemphrCount_Give", 200, NULL, 2, NULL);
	
	vTaskStartScheduler();          //开启任务调度
}

/*
 * 初始化各个模块
*/
void systemInit(void)
{
	SysTick_Init(72);
	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	USART1_Init(115200);
	printf("串口初始化成功！\r\n");

	/* 初始化计数信号量 : 要将 configUSE_COUNTING_SEMAPHORES 宏置1 */
	vSemaphoreCount = xSemaphoreCreateCounting(1, 0);
}
