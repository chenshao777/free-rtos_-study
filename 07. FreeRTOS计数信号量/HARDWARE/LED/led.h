#ifndef _LED_H_
#define _LED_H_

#include "stm32f10x.h"

#define ON  1
#define OFF 0

#define   LED1_GPIO_Pin      GPIO_Pin_13	
#define   LED2_GPIO_Pin      GPIO_Pin_7	
#define   LED3_GPIO_Pin      GPIO_Pin_8	
#define   LED1(n)    if(n)   GPIO_ResetBits(GPIOC,LED1_GPIO_Pin); \
										 else    GPIO_SetBits(GPIOC,LED1_GPIO_Pin);
#define   LED2(n)    if(n)   GPIO_ResetBits(GPIOF,LED2_GPIO_Pin); \
										 else    GPIO_SetBits(GPIOF,LED2_GPIO_Pin);										 
#define   LED3(n)    if(n)   GPIO_ResetBits(GPIOF,LED3_GPIO_Pin); \
										 else    GPIO_SetBits(GPIOF,LED3_GPIO_Pin);

void LED_Init(void);

#endif
