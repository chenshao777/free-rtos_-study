#ifndef _mq2_H
#define _mq2_H
#include "stm32f10x.h"
 
#define ADC1_DR_Address  ((uint32_t)0x4001244c);

extern __IO uint16_t ADC_ConvertedValue; 
extern char mq2_count;

void adc_init(void);
float MQ2_GetPPM(void);
 
#endif
