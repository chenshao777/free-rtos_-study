#include "stm32f10x.h"
#include "led.h"
#include "usart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "SysTick.h"
#include "queue.h"
#include "string.h"
#include "stdlib.h"


/* 定义 QueueHandle_t 类型变量，用于保存队列句柄 */
QueueHandle_t xQueue;

char sendBuff[200];

/*
 * 写入队列大块数据 
 */
void taskSend(void *pvParameters)
{
	static int count = 0;
	BaseType_t res;
	char *buff;
	
	for(;;)
	{
		/* 向缓冲区里放数据 */
		sprintf(sendBuff, "My name is ChenShao %d\r\n", count++);
		buff = sendBuff;
		
		printf("sendBuff = 0x%016lx\r\n", (long unsigned int)sendBuff);
		printf("&sendBuff = 0x%016lx\r\n", (long unsigned int)&sendBuff);
		
		printf("buff = 0x%016lx\r\n", (long unsigned int)buff);
		printf("&buff = 0x%016lx\r\n", (long unsigned int)&buff);
		/*
		 * 这里不能传入 sendBuff、&sendBuff、buff ，因为这里需要传入一个指向一片内存的指针的地址
		 * 而sendBuff、&sendBuff、buff都是数组首地址，首地址没有指向任何内存
		*/
		res = xQueueSendToBack(xQueue, &buff, 0);  
		if(res != pdPASS){
			printf("write fail \r\n");
		}
	}
}

/*
 * 读取队列大块数据 
 */
void taskRead(void *pvParameters)
{
	const TickType_t delay = 100 / portTICK_PERIOD_MS;
	char *str;
	BaseType_t res;
	for(;;)
	{
		res = xQueueReceive(xQueue, &str, delay);
		if(res == pdPASS){
			printf("read data : %s\r\n", str);
		}else{
			printf("read null\r\n");
		}
	}
}

int main()
{
	SysTick_Init(72);
	USART1_Init(115200);
	printf("串口初始化成功！\r\n");
	
	/* 创建队列，1个字节，仅用于保存大块数据首地址 */
	xQueue = xQueueCreate(1, sizeof(char *));
	
	xTaskCreate(taskSend, "taskSend", 1000, NULL, 1, NULL);
	
	xTaskCreate(taskRead, "taskRead", 1000, NULL, 2, NULL);
	
	/* 启动调度器 */
	vTaskStartScheduler();

}

