#include "stm32f10x.h"
#include "led.h"
#include "usart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "SysTick.h"
#include "queue.h"
#include "string.h"


/* 定义数据结构体：包含数据源ID & 数据 */
typedef struct{
	char id;
	uint32_t data;
}SendStruct;

#define	 task1_id     1 
#define  task2_id     2 
#define  task1_data   10 
#define  task2_data   20 

/* 任务发送到队列数据的结构体实例 */
const SendStruct xSend_Buff[2] = 
{
	{task1_id, task1_data},
	{task2_id, task2_data}
};

/* 创建队列返回句柄 */
QueueHandle_t Queue_t;

/*
  写队列
*/
int flag1 = 0;
int flag2 = 0;
int flag3 = 0;
void TaskSend(void *pvParameters)
{
	BaseType_t status;

	for(;;)
	{
		/* 写队列，参数：目标队列的句柄， 发送数据的指针， 阻塞超时时间*/
		status = xQueueSendToBack(Queue_t, pvParameters, 10);
		
		/* 如果写队列失败，则打印信息 */
		if(status != pdPASS){
			printf("send fail %d \r\n", ((SendStruct *)pvParameters)->id);
		}
		
		/* 判断自己是哪个任务 */
		if(((SendStruct *)pvParameters)->id == task1_id){
			flag1 = 1;
			flag2 = 0;
		}else{
			flag1 = 0;
			flag2 = 1;
		}
		flag3 = 0;
		
		/* 允许其它发送任务执行。 taskYIELD()通知调度器现在就切换到其它任务，而不必等到本任务的时间片耗尽 */ 
//		taskYIELD(); 
	}
}

/*
  读队列
*/
void TaskRead(void *pvParameters)
{
	SendStruct read_data;
	BaseType_t result;
//	BaseType_t count;
//	TickType_t xLastExecutionTime;
//	xLastExecutionTime = xTaskGetTickCount(); 
	for(;;)
	{
		flag1 = 0;
		flag2 = 0;
		flag3 = 1;
		
		/* 查询队列中数据个数 */
//		count = uxQueueMessagesWaiting(Queue_t);
//		printf("c = %d\r\n", count);
		
		/* 读队列 */
		result = xQueueReceive(Queue_t, &read_data, 10);
		/* 读到了数据 */
		if(result == pdPASS)
		{
			printf("ID: %d  DATA: %d\r\n", read_data.id, read_data.data);
		}
		else
			printf("Read NULL\r\n");
	}
}


int main()
{
	SysTick_Init(72);
	USART1_Init(115200);
	printf("串口初始化成功！\r\n");
	
	/*  创建队列, 用于保存最多5个值，每个数据单元都有足够的空间来存储一个 int 型变量  */
	Queue_t = xQueueCreate(5, sizeof(SendStruct));
	if(Queue_t != NULL)
		printf("队列创建成功！\r\n");
	
	/* 创建任务写队列 */
	xTaskCreate(TaskSend, "taskSend1", 200, (void *)&(xSend_Buff[0]), 3, NULL);
	xTaskCreate(TaskSend, "taskSend2", 200, (void *)&(xSend_Buff[1]), 3, NULL);
	
	
	/* 创建任务读队列 */
	xTaskCreate(TaskRead, "TaskRead", 200, NULL, 4, NULL);
	
	/* 启动调度器，任务开始执行 */
	vTaskStartScheduler();          //开启任务调度
}

