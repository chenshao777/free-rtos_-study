#include "stm32f10x.h"
#include "led.h"
#include "usart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "SysTick.h"
#include "dht11.h"
#include "mq2.h"
#include "semphr.h"
#include "string.h"

void systemInit(void);   //各模块初始化

float ADC_ConvertedValueLocal;  
QueueHandle_t xSemaphore;


/*
 * LED闪烁，表示系统正在运行
*/
void LED_Task(void *pvParameters)
{
	const TickType_t xDelay = 500 / portTICK_PERIOD_MS;
	u8 count = 0;
	while(1)
	{
		count++;
		if(count == 10)
		{
			count = 0;
			xSemaphoreGive(xSemaphore);  //给出二值信号量，解除任务vSemphr_Task
		}
		
		GPIOC->ODR ^= (1<<13);   //LED翻转
		vTaskDelay(xDelay);
	}
}


/*
 * 测试二值信号量任务
*/
void vSemphr_Task(void *pvParameters)
{
//	const TickType_t xDelay = 1000 / portTICK_PERIOD_MS;
//	const TickType_t xBlockTime = 6000 / portTICK_PERIOD_MS;  //等待信号量时间
	BaseType_t result;  //获取信号量结果变量
//	char str[10];       //解析串口数据缓冲区
	while(1)
	{
		/*
		 * portMAX_DELAY：表示没有超时限制
		 * 尝试获取信号量，如果没有获取到则进入阻塞态
		 * 如果设置了超时时间，超时时间内获取到了信号量，则返回pdPASS，如果没有获取到，则返回pdFALSE
		*/
		result = xSemaphoreTake(xSemaphore, portMAX_DELAY);   
		printf("***********信号量测试***********\r\n");
		if(result == pdPASS)
		{
			printf("获取到二值信号量！！！\r\n");
			
//			//截取串口数据
//			strncpy(str, (const char*)&USART1_RX_BUF[1], LEN-2);
//			printf("接收到 ： %s\r\n",str);
//			
//			//判断是否是led1命令
//			if(strcmp((const char*)str, "led1") == 0)
//			{
//				printf("LED反转\r\n");
//				GPIOC->ODR ^= (1<<13);
//			}
//			
//			//清空串口配置变量
//			memset(USART1_RX_BUF, 0, USART1_REC_LEN);
//			USART1_RX_STA = 0;
//			LEN = 0;
		}
		
//		vTaskDelay(xDelay);
	}
}

int main()
{
	systemInit();   //初始化各个模块
	
	// LED 显示系统正在运行 
	xTaskCreate(LED_Task, "LED_Task", 200, NULL, 1, NULL);
	
	// 测试二值信号量任务 
	xTaskCreate(vSemphr_Task, "vSemphr_Task", 200, NULL, 1, NULL);
	
	vTaskStartScheduler();          //开启任务调度
}

/*
 * 初始化各个模块
*/
void systemInit(void)
{
	SysTick_Init(72);
	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	USART1_Init(115200);
	printf("串口初始化成功！\r\n");
	
	/* 各模块初始化 */
	LED_Init();

	/* 初始化二值信号量 */
	vSemaphoreCreateBinary(xSemaphore);  
	/* 经过测试，初始化二值信号量后，默认信号量是满状态，可以先Take一下，把信号量清空 */
	xSemaphoreTake(xSemaphore, 0);     // 0表示不等待
	
}
